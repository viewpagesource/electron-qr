# ELECTRON QR

Installation
------------
To being with run

~~~bash
npm install
~~~

This will install all the necessary node modules needed to run angular and electron.

Building
--------
To build the application you can run one of two commands

1.  `npm run start` <-- This will build the angular app with JIT and deploy it in electron shell

2.  `npm run build` <-- This will build the angular app with AOT and deploy it in electron shell

Package and Deploy
------------------
To deploy the application as an executable run the following command

~~~bash
npm run deploy
~~~

This is build the application using AOT and package the application using the electron packager into and `asar` file.