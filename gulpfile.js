var gulp = require('gulp');
var uglify = require('gulp-uglify');
var pump = require('pump');
var sequence = require('gulp-sequence');
var rename = require('gulp-rename');

gulp.task('copy-package', function(){
  return gulp.src([
    './package.deploy.json'
  ])
  .pipe(rename("package.json"))
  .pipe(gulp.dest('dist'));
});

gulp.task('uglify-js', function (cb) {
  pump([
    gulp.src([
        'dist/*.js',
        '!dist/electron.js'
        ]),
    uglify(),
    gulp.dest('dist')
  ], cb);
});

gulp.task('default', sequence(['uglify-js','copy-package']));