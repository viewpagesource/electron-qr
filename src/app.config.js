module.exports = {
  "clientConfig": {
    //   The time period for which the splash screen needs to be displayed
    "splashTimeout": 2000,
    // The background image url for the slash screen
    "splashScreenBg": "assets/images/splash-screen-bg.png",
    // The brand logo image url for the slash screen 16x9 aspect ratio
    "splashScreenLogo": "assets/images/sr-logo-16x9.png",
    // The background image url for the login screen
    "loginBg": "assets/images/splash-screen-bg.png",
    // The brand logo image url for the login screen 16x9 aspect ratio
    "loginBrandLogo": "assets/images/sr-logo-16x9.png",
    // The event logo image url for the login screen 1x1 aspect ratio
    "loginEventLogo": "assets/images/summit-2017.png",
    // The text that appears below the login button
    "loginFormText": "Welcome to XT Summit 2017",
    // The brand/company name that appears in the footer
    "footerBrandName": "SapientRazorfish"
  },
  "serverConfig": {
    "app": {
      "name": "Electron QR"
    },
    "oAuth": {
      "clientId": "221026750825-6s74r6pa2qu0iqbdebel5uhlfo5b4p31.apps.googleusercontent.com",
      "profileUrl": "https://www.googleapis.com/userinfo/v2/me",
      "authorizationUrl": "https://accounts.google.com/o/oauth2/v2/auth",
      "tokenUrl": "https://www.googleapis.com/oauth2/v4/token",
      "redirectUri": "http://localhost"
    }
  }
}
