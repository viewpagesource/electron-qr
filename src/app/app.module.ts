import { ComponentsModule } from './components/components.module';
import { SplashScreenComponent } from './components/splash-screen/splash-screen.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { FormsModule } from '@angular/forms';
import { DataService } from './services/data.service';

@NgModule({
  imports: [
    FormsModule,
    BrowserModule,
    ComponentsModule,
    AppRoutingModule
  ],
  providers: [DataService],
  bootstrap: [AppComponent],
  declarations: [AppComponent]
})
export class AppModule { }
