import { OnInit } from '@angular/core/core';
import { Injectable } from '@angular/core';
const Instascan = require('instascan');

@Injectable()
export class ScannerService implements OnInit{
  scanner: any;
  cameras: any;
  constructor() {
     Instascan.Camera.getCameras().then((cameras) => {
      if (cameras.length > 0) {
        //this.scanner.start(cameras[0]);
        console.log(cameras);
        this.cameras = cameras;
      } else {
        console.error('No cameras found.');
      }
    }).catch((e) => {
      console.error(e);
    });
  }
  ngOnInit(){}

}
