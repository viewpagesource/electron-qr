import { DataService } from './services/data.service';
import { NetworkService } from './services/network.service';
import { ConfigService } from './services/config.service';
import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
//To prevent conflict between webpack and electron require
const { ipcRenderer } = window['require']('electron');

@Component({
  selector: 'qr-app',
  templateUrl:'./app.component.html', 
  styles: [],
  host:{
    '[class]':'hostClass'
  }
})
export class AppComponent implements OnInit {
  copywriteYear:number = (new Date()).getFullYear();
  footerBrandName: string = '';
  hostClass:string = '';
  constructor(
    private changeDetectorRef: ChangeDetectorRef,
    private configService: ConfigService,
    private networkService: NetworkService,
    private dataService: DataService
  ) { }

  ngOnInit() {
    this.footerBrandName = this.configService.getClientConfig().footerBrandName;

    ipcRenderer.on('window.network.up', () => {
      this.hostClass = '';
      this.networkService.setNetworkStatus('up');
    });

    ipcRenderer.on('window.network.down', () => {
      this.hostClass = 'app-offline';
      this.networkService.setNetworkStatus('down');
    });

    ipcRenderer.on('window.scanner.scanned', (evt, data) => {
      this.dataService.addQRData(data);
    });

    ipcRenderer.on('window.fetched.data', (evt, data) => {
      this.dataService.fetchSheetData(data);
    });
  }
}
