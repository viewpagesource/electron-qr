import { Injectable } from '@angular/core';

@Injectable()
export class NetworkService {
  status: string = 'up';
  constructor() { }
  
  isOnline() {
    return (this.getNetworkStatus() === 'up') ? true : false;
  }

  setNetworkStatus(status){
    this.status = status;
  }
  
  getNetworkStatus(){
    return this.status;
  }

}
