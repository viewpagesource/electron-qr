import { DataService } from './data.service';
import { NetworkService } from './network.service';
import { AuthResolverService } from './auth-resolver.service';
import { ConfigService } from './config.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [],
  providers:[ConfigService, AuthResolverService, NetworkService, DataService]
})
export class ServicesModule { }