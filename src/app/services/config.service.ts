import { Injectable } from '@angular/core';
 
import { clientConfig } from '../../app.config';

@Injectable()
export class ConfigService {

  constructor() { }

  getClientConfig(): any {
    return clientConfig || {};
  }

}
