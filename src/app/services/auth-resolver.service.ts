import { Resolve, Router, ActivatedRouteSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
const { ipcRenderer } = window['require']('electron');

@Injectable()
export class AuthResolverService implements Resolve<any>{

  constructor(
    private router: Router
  ) { }
  
  redirectToLogin(){
    console.log(`Redirecting to Login!`);
    this.router.navigate(['startup','login']);
  }
  
  redirectToDashboard(){
    console.log(`Redirecting to Dashboard!`);
    this.router.navigate(['dashboard']);
  }

  resolve(route: ActivatedRouteSnapshot) {
    let page = route.data['page'];
    return new Promise((resolve, reject) => {
      ipcRenderer.send('window.login.getAuth');
      ipcRenderer.on('window.login.authData', (evt, data) => {
        resolve(data);
      });
    }).then((authData: any) => {
      if(page === 'login'){
        if(authData.access_token){
          this.redirectToDashboard();
          return null;
        }
        return authData;
      } else {
        if(!authData.access_token){
          this.redirectToLogin();
          return null;
        }
        return authData;
      }
    });
  }
}
