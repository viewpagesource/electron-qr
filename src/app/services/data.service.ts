import { OnInit } from '@angular/core';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
const { ipcRenderer } = window['require']('electron');

@Injectable()
export class DataService implements OnInit{
  private qrDataObservable: Observable<any>;
  private qrDataObserver;
  private qrData: any[] = [];
  constructor() {
    this.qrDataObservable = new Observable(observer => this.qrDataObserver = observer);
  }

  ngOnInit() {
  }

  clearQRData(): void {
    this.qrData = [];
    this.qrDataObserver.next(this.qrData);
  }

  getQRData(): Observable<any> {
    return this.qrDataObservable;
  }

  addQRData(data: any) {
    this.qrData.unshift([{
        formattedValue: data.name
      }, {
        formattedValue: data.email
      },
      {
        formattedValue: data.ntId
      },
      {
        formattedValue: data.contact
      },
      {
        formattedValue: data.timestamp
      }
    ]);
    this.qrDataObserver.next(this.qrData);
  };

  fetchSheetData(data: any) {
    let sheetData = [];

    if(data.errors) {
      ipcRenderer.send('window.ask.sheetDetails');
    }

    data.map((item) => {
      sheetData.push(item.values);
    });

    this.qrData = sheetData;
    this.qrDataObserver.next(this.qrData.reverse());
  }

  getSheetHash(url: string): string | boolean {
    // Check if spreadsheet URL
    if (url.indexOf('https://docs.google.com/spreadsheets/d/') < 0) {
      return false;
    }
    // Only retrieve the hash
    const hash = url.match(/\/([0-9]|[A-Z])\w+\//);
    if(hash) {
      return hash[0].replace(/\//g,'');
    }
    return false;
  }
}
