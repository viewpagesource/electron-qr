import { ManualEntryComponent } from './components/manual-entry/manual-entry.component';
import { QrDataComponent } from './components/qr-data/qr-data.component';
import { SettingsComponent } from './components/settings/settings.component';
import { AuthResolverService } from './services/auth-resolver.service';
import { LoginComponent } from './components/login/login.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { SplashScreenComponent } from './components/splash-screen/splash-screen.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { StartupComponent } from './components/startup/startup.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path:'',
    redirectTo:'startup',
    pathMatch:'full'
  },{
    path:'startup',
    component: StartupComponent,
    children:[
      {
        path:'',
        redirectTo:'splash',
        pathMatch:'full'
      },
      {
        path:'splash',
        component: SplashScreenComponent
      },{
        path:'login',
        component: LoginComponent,
        data:{
          page: 'login'
        },
        resolve:{
          authData: AuthResolverService
        }
      }
    ]
  },{
    path:'dashboard',
    component: DashboardComponent,
    resolve:{
      authData: AuthResolverService
    },
    children:[
      {
        path:'',
        pathMatch:'full',
        component: QrDataComponent
      },
      {
        path:'home',
        pathMatch:'full',
        component: QrDataComponent

      },
      {
        path:'settings',
        pathMatch:'full',
        component: SettingsComponent

      },
      {
        path:'manualentry',
        pathMatch:'full',
        component: ManualEntryComponent

      }
    ]
  },
  { path: '**', component: NotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
