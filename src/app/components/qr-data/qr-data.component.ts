import { DataService } from '../../services/data.service';
import { Component, OnInit } from '@angular/core';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/startWith'
import { Observable }  from 'rxjs/Observable';
const { ipcRenderer } = window['require']('electron');

@Component({
  selector: 'qr-qr-data',
  templateUrl: './qr-data.component.html',
  styleUrls: ['./qr-data.component.scss']
})
export class QrDataComponent implements OnInit {
  qrData$: Observable<any[]>;
  constructor(
    private dataService: DataService
  ) { }

  ngOnInit() {
   this.qrData$ = this.dataService.getQRData()
   .map(res => res)
   .startWith(['No Data!']);
  }

  getDataDetails() {
    ipcRenderer.send('window.fetch.data');
  }

}
