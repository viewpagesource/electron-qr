import { NetworkService } from '../../services/network.service';
import { Router } from '@angular/router';
import { ConfigService } from '../../services/config.service';
import { Component, OnInit } from '@angular/core';
const { ipcRenderer } = window['require']('electron');

@Component({
  selector: 'qr-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  host: {
    class: 'relative flex flex-auto overflow-y-auto'
  }
})
export class LoginComponent implements OnInit {
  loginBrandLogoStyles: any = {};
  loginEventLogoStyles: any = {};
  loginFormText: string = '';
  loginButtonDisabled: boolean = false;
  constructor(
    private configService: ConfigService,
    private networkService: NetworkService,
    private router: Router
    ) { }

  ngOnInit() {

    this.loginFormText = this.configService.getClientConfig().loginFormText;

    this.loginBrandLogoStyles = {
      'background-image':`url(${this.configService.getClientConfig().loginBrandLogo})`
    };

    this.loginEventLogoStyles = {
      'background-image':`url(${this.configService.getClientConfig().loginEventLogo})`
    };

    ipcRenderer.once('window.login.success',() => {
      this.router.navigate(['/dashboard']);
    });
  }

  handleLogin() {
    if(this.networkService.isOnline()){
      ipcRenderer.send('window.login.open');
    }
  }
}
