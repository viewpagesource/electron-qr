import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DataService } from '../../services/data.service';
const { ipcRenderer } = window['require']('electron');

@Component({
  selector: 'qr-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {
  validUrl = true;
  placeholder = 'Please enter a valid Google sheets URL';
  constructor( private router: Router, private dataService: DataService) { }

  ngOnInit() {
    ipcRenderer.on('window.get.sheet', (evt, data) => {
      this.placeholder = `https://docs.google.com/spreadsheets/d/${data}`;
    });
    ipcRenderer.send('window.get.sheet');

  }

  mockSubmit(){
    return false;
  }

  saveSheetDetails(url: string): void {
    const sheetHash = this.dataService.getSheetHash(url);
    if (sheetHash) {
      ipcRenderer.send('window.send.sheet', sheetHash);
      ipcRenderer.send('window.fetch.data');
      this.router.navigate(['dashboard', 'home']);
      return;
    }
    this.validUrl = false;
    return;
  }

}
