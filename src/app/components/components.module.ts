import { RouterModule } from '@angular/router';
import { ServicesModule } from '../services/services.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SplashScreenComponent } from './splash-screen/splash-screen.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { StartupComponent } from './startup/startup.component';
import { QrDataComponent } from './qr-data/qr-data.component';
import { SettingsComponent } from './settings/settings.component';
import { ManualEntryComponent } from './manual-entry/manual-entry.component';

@NgModule({
  imports: [
    CommonModule,
    ServicesModule,
    RouterModule
  ],
  declarations: [LoginComponent, DashboardComponent, SplashScreenComponent, NotFoundComponent, StartupComponent, QrDataComponent,SettingsComponent, ManualEntryComponent],
  exports: [LoginComponent, DashboardComponent, SplashScreenComponent, NotFoundComponent, StartupComponent, QrDataComponent, SettingsComponent, ManualEntryComponent]
})
export class ComponentsModule { }
