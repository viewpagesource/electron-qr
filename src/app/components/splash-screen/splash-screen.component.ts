import { Router } from '@angular/router';
import { ConfigService } from '../../services/config.service';
import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';

@Component({
  selector: 'qr-splash-screen',
  templateUrl: './splash-screen.component.html',
  styleUrls: ['./splash-screen.component.scss']
})
export class SplashScreenComponent implements OnInit, OnDestroy {
  animateIn: boolean = false;
  fadeInTimeout: any;
  redirectTimeout: any;
  splashScreenLogoStyles: any = {};
  constructor(
    private configService: ConfigService,
    private router: Router
    ) {}

  ngOnInit(): void {

    //Setup logo styles
    this.splashScreenLogoStyles = {
      'background-image':`url(${this.configService.getClientConfig().splashScreenLogo})`
    };

    //Fade in the brand logo    
    this.fadeInTimeout = window.setTimeout(() => {
      this.animateIn = true;
      this.clearTimeouts();
      this.redirectTo(['startup','login']);
      // this.redirectTo(['404']);
      // this.redirectTo(['logged-in','dashboard']);
    },500);
  }

  ngOnDestroy(): void {
    this.clearTimeouts();
  }

  clearTimeouts(): void {
    if(this.fadeInTimeout){
      window.clearTimeout(this.fadeInTimeout);
    }
    if(this.redirectTimeout){
      window.clearTimeout(this.redirectTimeout);
    }
  }

  redirectTo(route: any[]){
    this.redirectTimeout = window.setTimeout(() => {
      this.router.navigate(route);
      this.clearTimeouts();
    },this.configService.getClientConfig().splashTimeout);
  }
}
