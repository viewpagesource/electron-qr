import { ConfigService } from '../../services/config.service';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'qr-startup',
  templateUrl: './startup.component.html',
  styleUrls: ['./startup.component.scss'],
  host:{
    class: 'relative flex flex-auto overflow-y-auto'
  }
})
export class StartupComponent implements OnInit {
  @ViewChild('startupScreen') startupScreen: ElementRef;
  startupScreenBgStyles: any = {};
  constructor(private configService: ConfigService) { }

  ngOnInit() {
    //Setup the background styles
    this.startupScreenBgStyles = {
      'background-image':`url(${this.configService.getClientConfig().splashScreenBg})`
    };

    let startupScreenCanvasID = this.startupScreen.nativeElement.id;

    if(this.pJSExists() && startupScreenCanvasID) {
      window['particlesJS'](startupScreenCanvasID, {
          'particles': {
            'number': {
              'value': 160,
              'density':{
                'enable': true,
                'value_area': 600
              }
            },
            'color': {
              'value': ['#FFFFFF']
            },
            'shape': {
              'type':'circle'
            },
            'size': {
              'value': 2,
              'random': true
            },
            'opacity': {
              'anim':{
                enable: true,
                speed: 1,
                opacity_min: 0
              },
              'value':1,
              'random': true
            },
            'line_linked': {
              'enable': false
            },
            'move':{
              'enable': true,
              'direction': 'none',
              'random': true,
              'straight':false,
              'speed':1,
              'out_mode':'out',
              'attract':{
                'enable': false
              }

            }
          },
          'interactivity': {
            'events':{
              'onhover':{
                enable:false
              },
              'onclick':{
                enable: false
              }
            }
          },
          'retina_detect':true
        });
      }
  }

  pJSExists(): boolean {
    return window['pJS'] ? true : false;
  }
}
