import { DataService } from '../../services/data.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
const { ipcRenderer } = window['require']('electron');

@Component({
  selector: 'qr-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  scannerOpen: boolean = false;
  scannerDisable: boolean = false;
  settingsOpen: boolean = false;
  homeOpen: boolean = true;
  manualEntry: boolean = false;
  gotData: boolean = true;

  constructor(
    private router: Router,
    private dataService: DataService
    ) { }

  ngOnInit() {
    ipcRenderer.once('window.login.loggedout', () => {
      this.router.navigate(['startup','login']);
    });

    //Using changeDetectorRef we are able to apply changes from events outside angular.
    ipcRenderer.on('window.scanner.opened',() => {
      this.scannerOpen = false;
      this.scannerDisable = true;
    });

    ipcRenderer.on('window.scanner.closed',() => {
      this.scannerOpen = false;
      this.scannerDisable = false;
    });

    ipcRenderer.on('window.show.spinner',() => {
      this.gotData = false;
    });

    ipcRenderer.on('window.hide.spinner',() => {
      this.gotData = true;
    });

    ipcRenderer.on('window.save.sheet',() => {
      this.handleSettings();
    });

    ipcRenderer.on('window.sheet.success',() => {
      this.dataService.clearQRData();
      this.scannerDisable = false;
      this.homeOpen = true;
      this.settingsOpen = false;
    });
  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.askSheetDetails()
    }, 100);
  }

  handleSettings() {
     this.router.navigate(['dashboard', 'settings']);
     this.settingsOpen = true;
     this.homeOpen = false;
     this.manualEntry = false;
  }

  handleManualEntry() {
    this.router.navigate(['dashboard', 'manualentry']);
    this.settingsOpen = false;
    this.homeOpen = false;
    this.manualEntry = true;
  }

  handleHome() {
    ipcRenderer.send('window.fetch.data');
    this.router.navigate(['dashboard', 'home']);
    this.homeOpen = true;
    this.settingsOpen = false;
    this.manualEntry = false;
  }

  handleLogout(){
    ipcRenderer.send('window.login.removeAuth');
  }

  askSheetDetails() {
    ipcRenderer.send('window.ask.sheetDetails');
  }

  openScannerWindow() {
    this.scannerOpen = true;
    this.scannerDisable = true;
    ipcRenderer.send('window.scanner.open');
  }

}
