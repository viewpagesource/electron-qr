import { Component, OnInit } from '@angular/core';
const { ipcRenderer } = window['require']('electron');

@Component({
  selector: 'qr-manual-entry',
  templateUrl: './manual-entry.component.html',
  styleUrls: ['./manual-entry.component.scss']
})
export class ManualEntryComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  addDataManually() {
    ipcRenderer.send('window.manual-browser.open');
  }

}
