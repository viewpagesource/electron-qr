import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'qr-not-found',
  templateUrl: './not-found.component.html',
  styleUrls: ['./not-found.component.scss'],
  host:{
    class: 'window-content'
  }
})
export class NotFoundComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
