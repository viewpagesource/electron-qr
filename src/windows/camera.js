const Instascan = require('instascan');
const { ipcRenderer } = require('electron');
const chalk = require('chalk');

const scanner = new Instascan.Scanner({
  video: document.getElementById('preview')
});

scanner.addListener('scan', function (content) {
  console.log(`Scanned object --> ${content}`);
  ipcRenderer.send('window.scanner.scanned', content);
});

Instascan.Camera.getCameras().then(function (cameras) {
  if (cameras.length > 0) {
    scanner.start(cameras[0]).then(function () {
      console.log(`Camera Acitive!`);
      ipcRenderer.send('window.scanner.camera.active');
    });
  } else {
    console.error(`No cameras found!`);
    ipcRenderer.send('window.scanner.camera.error', 'No cameras found');
  }
}).catch(function (e) {
  console.error(e);
  ipcRenderer.send('window.scanner.camera.error', 'No cameras found');
});

