//! IMPORTANT --> THIS FILE ONLY TO BE USED IN THE BROWSER WINDOW AND NOT IN NODE
const { ipcRenderer } = require('electron');
const updateOnlineStatus = () => {
  ipcRenderer.send('network.status', Offline.state);
}
Offline.on('up', updateOnlineStatus);
Offline.on('down', updateOnlineStatus);
Offline.options = {checks: {image: {url: 'http://www.placehold.it/1/1'}, active: 'image'}};