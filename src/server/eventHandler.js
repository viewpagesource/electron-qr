const { ipcMain } = require('electron');
const chalk = require('chalk');

module.exports = {
  errorEvent: (errMsg = 'An unknown error occured!') => {
    ipcMain.emit('window.main.error', new Error(errMsg));
    console.log(chalk.red(`Error event --> ${errMsg}`));
  },
  successEvent: (successMsg = 'Operation was successful!') => {
    ipcMain.emit('window.main.success', new Error(successMsg));
    console.log(chalk.green(`Success event --> ${successMsg}`));
  },
  customEvent: (channel = '', data = {}) => {
    if (channel) {
      ipcMain.emit(channel, data);
      console.log(chalk.blue(`Custom event --> channel:"${channel}", data:"${JSON.stringify(data)}"`));
      return;
    }
    console.log(chalk.red(`Custom channel not found!`));
  }
};
