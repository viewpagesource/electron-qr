const _ = require('lodash');
const path = require('path');
const url = require('url');
const fetch = require('node-fetch');
const chalk = require('chalk');
const querystring = require('querystring');
const { BrowserWindow } = require('electron');
const { serverConfig } = require('./config');
const eventHandler = require('./eventHandler');

let oAuthWindow,
  oAuthWindowParams,
  oAuthURLParams,
  oAuthOptions,
  oAuthTokenHeaders,
  oAuthTokenRequestParams,
  oAuthConfig;


const WINDOW_PARAMS = {
  width: 800,
  height: 600,
  show: false,
  alwaysOnTop: true,
  autoHideMenuBar: true,
  webPreferences: {
    nodeIntegration: false,
    webSecurity: false,
    allowRunningInsecureContent: true
  }
};

const OPTIONS = {
  scope: 'profile email openid https://spreadsheets.google.com/feeds'
};

const URL_PARAMS_DEFAULT = {
  response_type: 'code',
  redirect_uri: '',
  client_id: '',
  scope: {},
  access_type: ''
};

const TOKEN_HEADERS = {
  'Accept': 'application/json',
  'Content-Type': 'application/x-www-form-urlencoded'
};

const TOKEN_REQUEST_DATA = {
  code: '',
  grant_type: 'authorization_code',
  redirect_uri: '',
  client_id: ''
};


const oAuth = function () {
  this.setOAuthConfig(serverConfig.oAuth);
  this.setOAuthOptions(OPTIONS);
  this.setOAuthWindowParams(WINDOW_PARAMS);
  this.setOAuthTokenHeaders();
  this.setOAuthURLParams({
    redirect_uri: this.getOAuthConfig().redirectUri,
    client_id: this.getOAuthConfig().clientId,
    scope: this.getOAuthOptions().scope
  });
  console.log(chalk.green('Auth Module Initialized!'));
  return this;
}


oAuth.prototype.setOAuthWindowParams = function (options) {
  oAuthWindowParams = options;
};

oAuth.prototype.getOAuthWindowParams = function () {
  return oAuthWindowParams;
};

oAuth.prototype.setOAuthOptions = function (options) {
  oAuthOptions = options;
};

oAuth.prototype.getOAuthOptions = function () {
  return oAuthOptions;
};

oAuth.prototype.setOAuthConfig = function (options) {
  oAuthConfig = options;
};

oAuth.prototype.getOAuthConfig = function () {
  return oAuthConfig;
};

oAuth.prototype.setOAuthURLParams = function (options) {
  oAuthURLParams = querystring.stringify(Object.assign({}, URL_PARAMS_DEFAULT, _.pick(options, _.keys(URL_PARAMS_DEFAULT))));
};

oAuth.prototype.getOAuthURLParams = function (options) {
  return oAuthURLParams;
};

oAuth.prototype.setOAuthTokenHeaders = function () {
  oAuthTokenHeaders = TOKEN_HEADERS;
};

oAuth.prototype.getOAuthTokenHeaders = function () {
  return oAuthTokenHeaders;
};

oAuth.prototype.setOAuthTokenRequestParams = function (options) {
  oAuthTokenRequestParams = querystring.stringify(Object.assign({}, TOKEN_REQUEST_DATA, _.pick(options, _.keys(TOKEN_REQUEST_DATA))));
};

oAuth.prototype.getOAuthTokenRequestParams = function () {
  return oAuthTokenRequestParams;
};

oAuth.prototype.urlCallbackHandler = function (newUrl) {
  let urlParts = url.parse(newUrl, true);
  let query = urlParts.query;
  let code = query.code;
  let error = query.error;
  let href = urlParts.href;
 
  //In case the clicks on `Don't Allow`
  // if(href.indexOf(this.getOAuthConfig().redirectUri) > -1){
  //   if(!code){
  //     console.log(chalk.blue(`User denied permissions!`));
  //     this.destroyOAuthWindow();
  //     return;
  //   }
  // }

  if (code || error) {
    console.log(chalk.blue(`"code" or "error" is available in URL!`));
    this.destroyOAuthWindow();
  }

  if (code) {
    console.log(chalk.green(`Authentication code --> ${JSON.stringify(code)}`));
    this.getOAuthToken(code).then((data) => {
      eventHandler.successEvent(`Token recieved!`);
      eventHandler.customEvent(`window.login.success`, data);
    });
  }


  if (error) {
    console.log(chalk.red(`Authentication error --> ${JSON.stringify(error)}`));
    eventHandler.errorEvent(`Token not recieved!`);
  }
};

oAuth.prototype.getOAuthToken = function (code) {

  this.setOAuthTokenRequestParams({
    code: code,
    redirect_uri: this.getOAuthConfig().redirectUri,
    client_id: this.getOAuthConfig().clientId
  });

  console.log(this.getOAuthTokenRequestParams());

  return fetch(this.getOAuthConfig().tokenUrl, {
    method: 'POST',
    headers: this.getOAuthTokenHeaders(),
    body: this.getOAuthTokenRequestParams()
  })
  .then(res => res.json())
  .catch(err => err);
};

oAuth.prototype.createOAuthWindow = function () {
  oAuthWindow = new BrowserWindow(this.getOAuthWindowParams());
  oAuthWindow.loadURL(`${this.getOAuthConfig().authorizationUrl}?${this.getOAuthURLParams()}`);
  oAuthWindow.show();
  oAuthWindow.webContents.on('will-navigate', (evt, newUrl) => {
    this.urlCallbackHandler(newUrl);
  });
  oAuthWindow.webContents.on('did-get-redirect-request', (evt, oldUrl, newUrl) => {
    this.urlCallbackHandler(newUrl);
  });
  oAuthWindow.once('closed', () => {
    this.destroyOAuthWindow();
  });
};


oAuth.prototype.destroyOAuthWindow = function () {
  if(oAuthWindow){
    if(!oAuthWindow.isDestroyed()){
      oAuthWindow.destroy();
    } else {
      oAuthWindow = null;
    }
  }
};

oAuth.prototype.create = function () {
  this.createOAuthWindow();
  return this;
};

oAuth.prototype.destroy = function () {
  this.destroyOAuthWindow();
  return this;
};

module.exports = new oAuth();
