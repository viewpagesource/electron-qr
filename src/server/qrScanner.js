const { BrowserWindow } = require('electron');
const path = require('path');
const url = require('url');
const chalk = require('chalk');
const eventHandler = require('./eventHandler');

const WINDOW_SIZE = {
  width: 300,
  height: 300
};

const WINDOW_PARAMS = {
  width: WINDOW_SIZE.width,
  height: WINDOW_SIZE.height,
  resizable: false,
  maximizable: false,
  title: 'Scanner Cam'
};

let qrScannerWindow,
  qrScannerWindowParams,
  qrScannerTemplate;

const qrScanner = function () {
  this.setQrScannerWindowParams();
  this.setQrScannerTemplate();
};

qrScanner.prototype.setQrScannerWindowParams = function () {
  qrScannerWindowParams = WINDOW_PARAMS;
};

qrScanner.prototype.getQrScannerWindowParams = function () {
  return qrScannerWindowParams;
};

qrScanner.prototype.setQrScannerTemplate = function () {
  qrScannerTemplate = url.format({
    pathname: path.join(__dirname, '..', 'windows', 'camera.html'),
    protocol: 'file:',
    slashes: true
  });
};

qrScanner.prototype.getQrScannerTemplate = function () {
  return qrScannerTemplate;
};

qrScanner.prototype.createQrScannerWindow = function () {
  qrScannerWindow = new BrowserWindow(this.getQrScannerWindowParams());
  qrScannerWindow.loadURL(this.getQrScannerTemplate());
  qrScannerWindow.on('show', () => {
      eventHandler.customEvent('window.scanner.opened');
  });
  qrScannerWindow.once('closed', () => {
    eventHandler.customEvent('window.scanner.closed');
    this.destoryQrScannerWindow();
  });
};


qrScanner.prototype.destoryQrScannerWindow = function () {
  if (qrScannerWindow) {
    if (!qrScannerWindow.isDestroyed()) {
      qrScannerWindow.destroy();
    } else {
      qrScannerWindow = null;
    }
  }
};

qrScanner.prototype.create = function(){
    this.createQrScannerWindow();
};

qrScanner.prototype.destroy = function(){
    this.destoryQrScannerWindow();
};

module.exports = new qrScanner();
