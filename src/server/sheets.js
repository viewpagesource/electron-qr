var google = require('googleapis');
var googleAuth = require('google-auth-library');
var util = require('util');

var SheetsHelper = function (accessToken) {
  
  var authClient = new googleAuth();
  var auth = new authClient.OAuth2();
  auth.credentials = {
    access_token: accessToken
  };
  this.service = google.sheets({
    version: 'v4',
    auth: auth
  });
};

module.exports = SheetsHelper;
var COLUMNS = new Array(5);
SheetsHelper.prototype.sync = function (spreadsheetId, sheetId, orders, callback) {
  var requests = [];
  // Resize the sheet.
  requests.push({
    updateSheetProperties: {
      properties: {
        sheetId: sheetId,
        gridProperties: {
          columnCount: COLUMNS.length
        }
      },
      fields: 'gridProperties(columnCount)'
    }
  });
  // Set the cell values.
  requests.push({
    appendCells: {
      sheetId: sheetId,
      rows: {
        values: orders
      },
      fields: '*'
    }
  });
  // Send the batchUpdate request.
  var request = {
    spreadsheetId: spreadsheetId,
    resource: {
      requests: requests
    }
  };
  this.service.spreadsheets.batchUpdate(request, function (err) {
    if (err) {
      console.log(err)
      return callback(err);
    }
    return callback();
  });
};

SheetsHelper.prototype.getDetails = function (spreadsheetId, callback) {
  var request = {
    spreadsheetId: spreadsheetId,
    ranges: 'A:Z',
    includeGridData: true
  };

  this.service.spreadsheets.get(request, function (err, response) {
    if (err) {
      console.log(err)
      return callback(err);
    }
    const userData = response.sheets[0].data[0].rowData;
    return callback(userData);
  });
};
