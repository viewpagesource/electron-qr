const {app, BrowserWindow, ipcMain, dialog, remote, shell} = require('electron');
const path = require('path');
const url = require('url');
const chalk = require('chalk');
const qrScanner  = require('./server/qrScanner');
const oAuth  = require('./server/oAuth');
const storage = require('electron-json-storage');
const eventHandler = require('./server/eventHandler');
const { serverConfig } = require('./app.config');
//const CryptoJS = require('crypto-js');
//const cypher = require('../package.json').cypher;
const SheetsHelper = require('./server/sheets');

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let win;

function createWindow() {
  // Create the browser window.
  win = new BrowserWindow({
    width: 800,
    height: 600,
    resizable: false,
    maximizable: false,
    title: serverConfig.app.name
  })

  // and load the index.html of the app.
  win.loadURL(url.format({
    pathname: path.join(__dirname, 'index.html'),
    protocol: 'file:',
    slashes: true
  }))

  // Open the DevTools.
  // win.webContents.openDevTools() // <-- Un comment to debug

  // Emitted when the window is closed.
  win.on('closed', () => {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    win = null;
    ipcMain.emit('window.main.closed');
  });
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow)

// Quit when all windows are closed.
app.on('window-all-closed', () => {
  // On macOS it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (win === null) {
    createWindow()
  }
})

//Important since home realm has certificate issue
app.commandLine.appendSwitch("ignore-certificate-errors");

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.


//Check Online Events
ipcMain.on('network.status', (event, status) => {
  console.log(chalk.blue(`Network Status --> ${status}`));
  switch (status) {
    case 'up':
      if (win) {
        win.webContents.send('window.network.up');
      }
      break;
    case 'down':
      if (win) {
        win.webContents.send('window.network.down');
      }
      oAuth.destroy();
      qrScanner.destroy();
      break;
    default:
      break;
  }
});


//Login Events
ipcMain.on('window.login.open', () => {
  oAuth.create();
});

ipcMain.on('window.login.success', (data) => {
  oAuth.destroy();
  storage.set('oAuth', data, (err) => {
    if (err) {
      eventHandler.errorEvent(`Error storing authorization data!`);
      return;
    }
    console.log(chalk.green(`Authorization data saved!`));
    win.webContents.send('window.login.success', data);
  });
});

//Retrieve Auth Data
ipcMain.on('window.login.getAuth', () => {
  storage.get('oAuth', (err, data) => {
    if (err) throw err;
    console.log(chalk.green(`Auth data retrieved!`));
    if (win) {
      win.webContents.send('window.login.authData', data);
    }
  });
});

//Remove Auth Data
ipcMain.on('window.login.removeAuth', () => {
  storage.remove('oAuth', (err) => {
    if (err) throw err;
    console.log(chalk.green(`Auth data removed!`));
    if (win) {
      win.webContents.send('window.login.loggedout');
    }
  });
});

//QR Scanner Events
ipcMain.on('window.scanner.open', () => {
  qrScanner.create();
});

ipcMain.on('window.scanner.opened', () => {
  if (win) {
    win.webContents.send('window.scanner.opened');
  }
});

ipcMain.on('window.scanner.closed', () => {
  if (win) {
    win.webContents.send('window.scanner.closed');
  }
});

ipcMain.on('window.send.sheet', (res, data) => {
  storage.set('sheetDetails', data, (err) => {
    if (err) {
      eventHandler.errorEvent(`Error storing authorization data!`);
      return;
    }
    console.log(chalk.green(`Sheet data saved!`));
    win.webContents.send('window.sheet.success', data);
  });
});

ipcMain.on('window.get.sheet', (evt) => {
 storage.get('sheetDetails', (err, data) => {
   if (err) throw err;
   win.webContents.send('window.get.sheet', data);
 });
});

ipcMain.on('window.scanner.scanned', (evt, data) => {
  const userDetails =  JSON.parse(data); 
  // To parse encrypted data use below
  //JSON.parse(CryptoJS.AES.decrypt(data.toString(), cypher).toString(CryptoJS.enc.Utf8));

  if (userDetails.name) {
    
    userDetails.timestamp = Date.now().toString();
    
    if (win) {
      win.webContents.send('window.scanner.scanned', userDetails);
    }
    
    console.log(chalk.green(`QR DATA --> ${userDetails}`));

    storage.get('oAuth', (err, data) => {
      
      if (err) throw err;
      
      if (win) {
        win.webContents.send('window.login.authData', data);
      }

      const accessToken = data.access_token;

      let helper = new SheetsHelper(accessToken);

      storage.get('sheetDetails', (err, data) => {
        
        var orders = [{
            userEnteredValue: {
              stringValue: userDetails.name
            }
          },
          {
            userEnteredValue: {
              stringValue: userDetails.email
            }
          },
          {
            userEnteredValue: {
              stringValue: userDetails.ntId
            }
          },
          {
            userEnteredValue: {
              stringValue: userDetails.contact
            }
          },
          {
            userEnteredValue: {
              stringValue: userDetails.timestamp
            }
          }
        ];

        helper.sync(data, 0, orders, function (err, userData) {

          if (err) {
            if (err.code === 401) {
              var choice = dialog.showMessageBox({
                  type: 'error',
                  buttons: ['Login'],
                  title: 'Confirm',
                  message: `Opps! Session out. You need to log in`
                },
                (res) => {
                  if (res === 0) {
                    qrScanner.destroy();
                    storage.remove('oAuth', (err) => {
                      if (err) throw err;
                      console.log(chalk.green(`Auth data removed!`));
                      if (win) {
                        win.webContents.send('window.login.loggedout');
                      }
                    });
                  }
                }
              );

              return choice === 0;

            }
          };
          console.log(chalk.green(`Scaned data saved!`));

        });
      });

    });
  }
  console.log(data.toString());
});

ipcMain.on('window.ask.sheetDetails', () => {
  var choice = dialog.showMessageBox({
      type: 'info',
      buttons: ['Ok'],
      title: 'Confirm',
      message: `Let's save the sheet details`
    },
    (res) => {
      if (res === 0) {
        if (win) {
          win.webContents.send('window.save.sheet');
        }
      }
    }
  );

  return choice === 0;

});

ipcMain.on('window.manual-browser.open', () => {
  storage.get('sheetDetails', (err, data) => {
    shell.openExternal(`https://docs.google.com/spreadsheets/d/${data}`);
  })
});

ipcMain.on('window.fetch.data', () => {
  if (win) {
    win.webContents.send('window.show.spinner');
  }

  storage.get('oAuth', (err, data) => {
    if (err) throw err;
    if (win) {
      win.webContents.send('window.login.authData', data);
    }
    const accessToken = data.access_token;
    let helper = new SheetsHelper(accessToken);
    storage.get('sheetDetails', (err, data) => {
      
      if (err){
        throw err;
      } 

      helper.getDetails(data, function (response) {
        if (response) {
          if (response.code === 401) {
            var choice = dialog.showMessageBox({
                type: 'error',
                buttons: ['Login'],
                title: 'Confirm',
                message: `Opps! You need to log in`
              },
              (res) => {
                if (res === 0) {
                  storage.remove('oAuth', (err) => {
                    if (err) throw err;
                    console.log(chalk.green(`Auth data removed!`));
                    if (win) {
                      win.webContents.send('window.login.loggedout');
                    }
                  });
                }
              }
            );

            return choice === 0;

          } else if (response.port === 443) {
            win.webContents.send('window.hide.spinner');
            var choice = dialog.showMessageBox({
              type: 'error',
              buttons: ['Ok'],
              title: 'Confirm',
              message: `Can't update data! Check you network connection`
            });
            return choice === 0;

          } else {
            if (win) {
              win.webContents.send('window.fetched.data', response);
              win.webContents.send('window.hide.spinner');
            }
            console.log(chalk.green(`Data has been fetched!`));
          }
        } else {
          win.webContents.send('window.fetched.data', []);
          win.webContents.send('window.hide.spinner');
        }
      }, (error) => {
        throw error;
      });
    });
  });
});

